package rwlock

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

var a int = 10

var wm sync.WaitGroup

func Test1(t *testing.T) {
	go func() {
		err, s := Lock("test")
		t.Log(err, s)
	}()
	go func() {
		err, s := Lock("test")
		t.Log(err, s)
	}()
	go func() {
		err, s := Lock("test")
		t.Log(err, s)
	}()
	time.Sleep(time.Second * 5)
}

func Test2(t *testing.T) {
	RedisConn.Do("auth", "123456")
	RedisConn.Do("set", "test", "1")
	reply, err := RedisConn.Do("get", "test")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(reply)
}

var mset = make(map[string]string)

func Test3(t *testing.T) {
	//go func() {
	//	err, s := Lock("testm")
	//	t.Log(err, s)
	//	mset["test"] = "1"
	//	mset["test2"] = "2"
	//	err = Unlock("test", *s)
	//	t.Log(err)
	//}()

	err, s := Lock("test11")
	t.Log("-------", err, *s)
	mset["test"] = "3"
	mset["test2"] = "4"
	// Unlock("test11", *s)
	fmt.Println(mset)
	err1, s1 := Lock("test11")
	if err1 != nil {
		t.Fatal(err1, s1)
	}
	mset["test3"] = "5"
	mset["test4"] = "6"
	fmt.Println(mset)
}
