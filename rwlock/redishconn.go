package rwlock

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
)

type redisClient struct {
	redis.Conn
}

var RedisConn redisClient

type Config struct {
	Host     string
	Port     string
	Password string
}

func Init(conf *Config) {
	address := conf.Host + ":" + conf.Port
	c, err := redis.Dial("tcp", address)
	if err != nil {
		fmt.Println("conn redis failed,", err)
		return
	}
	RedisConn = redisClient{c}
	RedisConn.Do("auth", conf.Password)
}

func (c *redisClient) Close() {
	c.Close()
}
