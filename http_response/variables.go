package http_response

import "errors"

var (
	ServerErr = NewErrResponse(500, errors.New("服务器错误"), nil)
	ClientErr = NewErrResponse(400, errors.New("客户端错误"), nil)
	MysqlErr  = NewErrResponse(500, errors.New("数据库错误"), nil)
	RedisErr  = NewErrResponse(500, errors.New("redis错误"), nil)
	NoData    = NewErrResponse(404, errors.New("没有数据"), nil)
)

var (
	ServerErrCode = 500
	ClientErrCode = 400
	MysqlErrCode  = 500
	RedisErrCode  = 500
	NoDataCode    = 404
)
