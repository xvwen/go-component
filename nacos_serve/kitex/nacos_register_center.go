package kitex

import (
	"github.com/cloudwego/kitex/pkg/rpcinfo"
	"github.com/cloudwego/kitex/server"
	"github.com/kitex-contrib/registry-nacos/registry"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/clients/naming_client"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
)

func RegisterCenterConfig(config ClientConfig, ipAndPort map[string]uint64) (naming_client.INamingClient, error) {
	var sc []constant.ServerConfig
	for k, v := range ipAndPort {
		tmp := *constant.NewServerConfig(k, v)
		sc = append(sc, tmp)
	}
	cc := constant.ClientConfig{
		NamespaceId:         config.NamespaceId,
		TimeoutMs:           config.TimeoutMs,
		NotLoadCacheAtStart: config.NotLoadCacheAtStart,
		LogDir:              config.LogDir,
		CacheDir:            config.CacheDir,
		LogLevel:            config.LogLevel,
		Username:            config.Username,
		Password:            config.Password,
	}
	cli, err := clients.NewNamingClient(
		vo.NacosClientParam{
			ClientConfig:  &cc,
			ServerConfigs: sc,
		},
	)
	if err != nil {
		return nil, err
	}
	return cli, err
}

func ServerBaseInfo(serviceName string) server.Option {
	return server.WithServerBasicInfo(&rpcinfo.EndpointBasicInfo{ServiceName: serviceName})
}

func NacosServerGroup(group string) registry.Option {
	return registry.WithGroup(group)
}
