package kitex

import (
	"testing"
)

func TestGetRemoteCinfog(t *testing.T) {
	remoteConfig := NacosConfig{
		Client: ClientConfig{
			Username:            "nacos",
			Password:            "nacos",
			NamespaceId:         "public",
			TimeoutMs:           5000,
			NotLoadCacheAtStart: true,
			LogDir:              "/tmp/nacos/log",
			CacheDir:            "/tmp/nacos/cache",
			LogLevel:            "debug",
		},
		Server: ServerConfig{
			Scheme:      "http",
			IpAddr:      "127.0.0.1",
			Port:        8848,
			ContextPath: "/nacos",
		},
		Param: ClientParam{
			DataId: "test",
			Group:  "DEFAULT_GROUP",
		},
	}

	var localConfig NacosConfig
	err := GetRemoteConfig[NacosConfig](remoteConfig, &localConfig)
	if err != nil {
		t.Error(err)
	}

	// localConfig ...

}

func TestNacosCneterConfig(t *testing.T) {
	config := ClientConfig{
		NamespaceId:         "test",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              "tmp/nacos/log",
		CacheDir:            "tmp/nacos/cache",
		LogLevel:            "debug",
		Username:            "nacis",
		Password:            "nacos",
	}
	centerConfig, err := RegisterCenterConfig(config, map[string]uint64{"127.0.0.1": 8848})
	if err != nil {
		t.Error(err)
	}

	_ = centerConfig

	//svr := demo.NewServer(
	//	new(StudentServiceImpl),
	//	server.WithServerBasicInfo(&rpcinfo.EndpointBasicInfo{ServiceName: "student"}),
	//	server.WithRegistry(registry.NewNacosRegistry(centerConfig, NacosServerGroup("test"))),
	//)
	//
	//// 启动服务
	//err = svr.Run()
	//if err != nil {
	//	log.Println(err.Error())
	//}

}
