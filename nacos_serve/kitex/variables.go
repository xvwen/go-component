package kitex

type NacosConfig struct {
	Client ClientConfig
	Server ServerConfig
	Param  ClientParam
}

type ClientConfig struct {
	NamespaceId         string
	TimeoutMs           uint64
	NotLoadCacheAtStart bool
	LogDir              string
	CacheDir            string
	LogLevel            string
	Username            string
	Password            string
}

type ServerConfig struct {
	IpAddr      string
	ContextPath string
	Port        uint64
	Scheme      string
}

type ClientParam struct {
	DataId string
	Group  string
}
