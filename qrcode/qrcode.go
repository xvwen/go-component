package main

import (
	"github.com/skip2/go-qrcode"
)

func CreateQRCodeWithUrl(url, filepath string) error {
	//encode, err := qrcode.Encode("https://www.baidu.com", qrcode.Medium, 256)
	//if err != nil {
	//	panic(err)
	//}
	err := qrcode.WriteFile(url, qrcode.Medium, 256, filepath)
	if err != nil {
		return err
	}
	return err

	// div的QR-code
	//code, err := qrcode.New("https://baidu.com", qrcode.Medium)
	//if err != nil{
	//	panic(err)
	//}
	//code.Content = ""
	//code.Level = qrcode.Low
	//code.BackgroundColor = color.Black
	//code.ForegroundColor = color.White
	//code.Write()
}
